import java.io.IOException;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Font {
  
  /** The texture that will hold the image details */
  private Texture texture;
  
  public void init() {
    // enable alpha blending
    GL11.glEnable(GL11.GL_BLEND);
    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
    
    try {
      texture = TextureLoader.getTexture("PNG", 
                               ResourceLoader.getResourceAsStream("font.png"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void render(String text, float x, float y, float z, float size) {

    Color.white.bind();
    texture.bind(); // or GL11.glBind(texture.getTextureID());

    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, 
                         GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, 
                         GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
    //GL11.glTexParameteri(GL11.GL_TEXTURE_2D, 
    //                     GL11.GL_TEXTURE_WRAP_S, GL11.GL_CLAMP);
    //GL11.glTexParameteri(GL11.GL_TEXTURE_2D, 
    //                     GL11.GL_TEXTURE_WRAP_T, GL11.GL_CLAMP);

    GL11.glEnable(GL11.GL_TEXTURE_2D);               
    GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);          
        
    GL11.glPushMatrix();

    GL11.glTranslatef(x,y,z);
    GL11.glScalef(size,size,0);

    for (int i = 0; i < text.length(); ++i)
      renderChar(text.charAt(i));

    GL11.glPopMatrix();

    GL11.glDisable(GL11.GL_TEXTURE_2D);               
  }

  private void renderChar(char ch) {
    final String chars = 
      "ABCDEFGHIJ" + 
      "KLMNOPQRST" + 
      "UVWXYZ.,:;" +
      "1234567890" +
      "?!-+*#$%/\\" +
      "abcdefghij" +
      "klmnopqrst" +
      "uvwxyz~'`\"" +
      "          " +
      "X         ";
    
    int i,j,c = chars.indexOf(ch);
    if (c == -1) {
      i = 0;
      j = 9; 
    } else {
      i = c % 10;
      j = c / 10;
    }
    float d = 6f/64f;
    float x = i * d;
    float y = j * d;
    
    GL11.glBegin(GL11.GL_QUADS);

    GL11.glTexCoord2f(x, y);
    GL11.glVertex2f(0, 0);

    GL11.glTexCoord2f(x+d, y);
    GL11.glVertex2f(1, 0);

    GL11.glTexCoord2f(x+d, y+d);
    GL11.glVertex2f(1, 1);

    GL11.glTexCoord2f(x, y+d);
    GL11.glVertex2f(0, 1);

    GL11.glEnd();
    
    GL11.glTranslatef(1,0,0);
  }
}