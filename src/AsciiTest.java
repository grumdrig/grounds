import java.io.DataInputStream;
import java.util.Arrays;


/** Visual test for Perlin Noise */
class AsciiTest {
  
  final int W;
  final int H;

  private static void usage() {
    System.err.print(
      "Usage: java AsciiTest [OPTS] ALGORITHM\n" +
      "OPTS:\n" +
      "  -s   Don't render shadows\n" +
      "  -r   Don't render rivers (when simulating rain)\n" +
      "  -1   Run just once and exit\n" +
      Algorithm.usage());
    System.exit(1);
  }

  public static void main(String[] args) {
    if (args.length < 1) usage();
    AsciiTest panel = new AsciiTest(args);
  }

  private Algorithm algorithm;

  boolean shadows = true;  // Render shadows?
  boolean rivers = true;   // Render rivers?
  int iterations = -1;     // Run forever by default

  public AsciiTest(String[] alg) {
    int i = 0;
    while (i < alg.length && alg[i].startsWith("-")) {
      if (alg[i].equals("-s")) shadows = !shadows;
      else if (alg[i].equals("-r")) rivers = !rivers;
      else if (alg[i].equals("-1")) iterations = 1;
      else usage();
      ++i;
    }
    algorithm = new Algorithm(Arrays.copyOfRange(alg, i, alg.length));
    W = algorithm.W;
    H = algorithm.H;
    for (i = 0; i < H+1; ++i)
      System.out.println();
    //System.out.print("\033[2J");
    while (iterations-- != 0) {
      render();
      try {
        // Enter quits
        DataInputStream dis = new DataInputStream(System.in) ;
        if (dis.available() > 0) {
          System.out.println("done");
          while (dis.available() > 0)
            System.in.read();
          iterations = 0;
        } else {
          Thread.sleep(1000/60);
        }
      } catch (Exception e) {
        break;
      }
    }
  }

  int frames = 0;
  long lastreport = 0;
  double plane = 0;

  private void render() {
    // Render terrain as ASCII
    algorithm.run();
    Terrain noise = algorithm.terrain();
    long now = System.currentTimeMillis();
    System.out.printf("\033[%dA", H+1);
    double z = now / 1000.0;
    for (int y = 0; y < H; y++) {
      for (int x = 0; x < W; x++) {
        double height = noise.noise(x, y);
        // Convert height to ARGB 
        String terra;
        int R = 0, G = 0, B = 0;
        String brightness = "2";
        // Cast shadow
        if (shadows && height > 0 && x > 0 && y > 0) {
          final int SUN = 6;
          int light = SUN;
          if (height < noise.noise(x-1,y-1)) light -= 2;
          if (height < noise.noise(x,y-1)) light -= 1;
          if (height < noise.noise(x-1,y)) light -= 1;
          if (light < SUN) 
            brightness = "3";
        }
        if (height < -0.5) {
          terra = "\033[0;34m~";
        } else if (height < 0) {
          terra = "\033[0;36m~";
        } else if (rivers && noise.water != null && 
                   noise.water[x+y*W] > noise.wmean + noise.wdev) {
          terra = "\033[0;26m~";
        } else if (height > 0.75) {
          terra = "\033[0;" + brightness + "7m^";
        } else  {
          terra = "\033[0;" + brightness + "2m#";
        }

        System.out.print(terra);
      }
      System.out.println("\033[0;0m");
    }

    // Report statistics
    ++frames;
    System.out.printf("%3.0f FPS\n", frames * 1000.0 / (now-lastreport));
    if (now - lastreport > 1000) {
      lastreport = now;
      frames = 0;
    }
  }


  private int RGB(int r, int g, int b) {
    if (r < 0) r = 0; else if (r > 255) r = 255;
    if (g < 0) g = 0; else if (g > 255) g = 255;
    if (b < 0) b = 0; else if (b > 255) b = 255;
    return 0xFF000000 | r << 16 | g << 8 | b;
  }
}
