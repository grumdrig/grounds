import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;


/** Visual test for Perlin Noise */
class PerlinNoiseTest extends JPanel {
  
  private static final int W = 256;
  private static final int H = 256;

  public static void main(String[] args) {
    JFrame frame = new JFrame("Perlin Noise Test");
    PerlinNoiseTest panel = new PerlinNoiseTest();
    frame.add(panel);
    frame.pack();
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private BufferedImage canvas;

  private PerlinNoise noise;

  public PerlinNoiseTest() {
    canvas = new BufferedImage(W, H, BufferedImage.TYPE_INT_ARGB);
    render();
  }

  int frames = 0;
  long lastreport = 0;

  private void render() {
    // Render noise
    double min = 0;
    double max = 0;
    long now = System.currentTimeMillis();
    double z = now / 1000.0;
    for (int x = 0; x < W; x++) {
      for (int y = 0; y < H; y++) {
        double height = noise.pinkNoise(x, y, z, W/2, 4, 0.5);
        canvas.setRGB(x, y, greyscale(height));
        if (min > height) min = height;
        if (max < height) max = height;
      }
    }

    // Report statistics
    ++frames;
    if (now - lastreport > 1000) {
      System.out.printf("%.3f - %.3f %3.0f FPS\n", min, max, 
                        frames * 1000.0 / (now-lastreport));
      lastreport = now;
      frames = 0;
    }
    repaint();
  }


  int heightToByte(double height) {
    // Convert from [-1.0, 1.0) to [0,256)
    // (Should properly be scaled by 128, I think, but noise function doesn't
    // generally reach the extreme ends, so using 160 to make it more dynamic.)
    int scaled = (int) (height * 160 + 128);
    if (scaled < 0) scaled = 0;
    else if (scaled > 255) scaled = 255;
    return scaled;
  }
      
  public int greyscale(double height) {
    // Convert height to ARGB as a simple black to white palette
    int scaled = heightToByte(height);
    return 0xFF000000 | scaled << 16 | scaled << 8 | scaled;
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    g2.drawImage(canvas, null, null);
    render();
  }

  public Dimension getPreferredSize() {
    return new Dimension(W,H);
  }
}
