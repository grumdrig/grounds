import java.io.File;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.util.Arrays;
import java.io.IOException;

class Render {
  
  public static void main(String[] args) throws IOException {
    File filename = new File(args[0]);
    Algorithm algorithm = new Algorithm(Arrays.copyOfRange(args, 1, 
                                                           args.length));
    algorithm.run();
    BufferedImage canvas = new BufferedImage(algorithm.W, algorithm.H, 
                                             BufferedImage.TYPE_INT_ARGB);
    render(canvas, algorithm, 0, true, false, false);
    ImageIO.write(canvas, "png", filename);
  }

  public static void render(BufferedImage canvas, Algorithm algorithm, 
                            long now, boolean shadows, boolean rivers,
                            boolean grayscale) {
    // Render terrain as an image
    final int W = algorithm.W;
    final int H = algorithm.H;
    Terrain noise = algorithm.terrain();
    double z = now / 1000.0;
    for (int x = 0; x < W; x++) {
      for (int y = 0; y < H; y++) {
        double height = noise.noise(x, y);
        // Convert height to ARGB 
        int R = 0, G = 0, B = 0;
        if (grayscale) {
          if (height < 0) {
            R = (int) ((height + 1.0) * 256.0);
          } else {
            R = G = B = (int) (height * 256.0);
          }
        } else {
          if (height < -0.5) {
            B = (int)((height + 1.0) * 2.0 * 256.0);
          } else if (height < 0) {
            G = (int)((height + 0.5) * 2.0 * 256.0);
            B = 255;
          } else if (rivers && noise.water != null && 
                     noise.water[x+y*W] > noise.wmean + noise.wdev) {
            R = (noise.water[x+y*W] > noise.wmean + noise.wdev * 2) ? 255 : 128;
          } else  {
            R = (int)(height * 255);
            G = 128 + (int)(height * 127);
            // For white peaks:
            //if (height > 0.75)
            //  B = (int)((height - 0.75) * 4.0 * 255);
          }
          // Cast shadow
          if (shadows && height > 0 && x > 0 && y > 0) {
            final int SUN = 6;
            int light = SUN;
            if (height < noise.noise(x-1,y-1)) light -= 2;
            if (height < noise.noise(x,  y-1)) light -= 1;
            if (height < noise.noise(x-1,y  )) light -= 1;
            if (light < SUN) {
              R = R * light / SUN;
              G = G * light / SUN; 
              //B = B * light / SUN;  removed to maybe add a blue cast to shadows
            }
          }
        }
        canvas.setRGB(x, y, RGB(R,G,B));
      }
    }
  }

  private static int RGB(int r, int g, int b) {
    if (r < 0) r = 0; else if (r > 255) r = 255;
    if (g < 0) g = 0; else if (g > 255) g = 255;
    if (b < 0) b = 0; else if (b > 255) b = 255;
    return 0xFF000000 | r << 16 | g << 8 | b;
  }


}
