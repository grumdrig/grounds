import java.util.Random;
import java.util.List;
import java.util.Stack;
import java.awt.Point;

class Algorithm {
  
  final int W;
  final int H;

  public static String usage() {
    return
      "ALGORITHM: [SIZE] [HEIGHT] GENERATOR [FILTER...]\n" +
      "SIZE (width) defaults to 256\n" +
      "HEIGHT defaults to SIZE\n" +
      "GENERATOR is one of:\n" +
      "  midpoint SCALE DIM    Midpoint displacement\n" +
      "  perlin MAXS MINS DIM  Perlin pink noise [64 1 .5]\n" +
      "  gustav MAXS MINS DIM  Gustavson pink noise [64 1 .5]\n" +
      "  fault NFAULTS         Fault noise\n" +
      "  spectral NWAVS DIM    Spectral synthesis\n" +
      "  voronoi C1 C2 NPTS    Voronoi diagram\n" +
      "  white                 White noise\n" +
      "  grid                  Filter test pattern\n" +
      "FILTER can be:\n" +
      "  smooth                Smooth on Von Neumann nbhd\n" +
      "  smoothmore            Smooth on Moore neighborhood\n" +
      "  normalize MIN MAX     Normalize heightmap\n" +
      "  binormalize MIN MAX   Normalize >0 and <0 sep'ly\n" +
      "  thermal ITERS THRESH  Thermal erosion\n" +
      "  olsen ITERS THRESH    Olsen erosion\n" +
      "  rain RAIN SOL EV CAP  Hydrologic erosion\n" +
      "  distort DEGREE SCALE  Apply lateral distortion\n" +
      "  fade RADIUS           Round off edges of map to -1 w/ RADIUS [10]\n" +
      "  exp EXP               Exponentiate positives [2]\n" +
      "  scale FACTOR          Multiply each point by FACTOR [-1]\n" +
      "  bias BIAS             Add BIAS to each point [1]\n" +
      "  step CUTOFF           Step function to 1 or 0 [0]\n" +
      "  abs                   Take the absolute value of each point\n" +
      "  fill                  Fill local minima\n" +
      "  path                  Build a path between two random points\n" +
      "STACK OPS:\n" +
      "  push                  Push terrain onto stack\n" +
      "  add                   Pop from terrain stack and add to current\n" +
      "  mul                   Pop from terrain stack and multiply current\n" +
      "LOOPING:\n" +
      "  loop | --             Begin loop: actions before this not repeated\n"+
      "(Filter and generator parameters are all optional)";
  }

  private Terrain terrain;

  Terrain terrain() { return terrain; }

  private String[] algorithm;
  int algStep = -1;
  int algStart = 0;

  private Stack<Terrain> stack;


  public Algorithm(String[] alg) {
    algorithm = alg;
    int width = 256, height = 256;
    try {
      height = width = Integer.parseInt(alg[algStart]);
      ++algStart;
      height = Integer.parseInt(alg[algStart]);
      ++algStart;
    } catch (NumberFormatException n) {
      // Ignore - doesn't start with a size argument
    }
    W = width;
    H = height;
    terrain = new Terrain(W, H);
  }

  private double algParam(double defaultValue) {
    if (algStep + 1 < algorithm.length) try {
        double result = Double.parseDouble(algorithm[algStep + 1]);
        ++algStep;
        return result;
      } catch (NumberFormatException e) { };
    return defaultValue;
  }

  private int algParam(int defaultValue) {
    if (algStep + 1 < algorithm.length) try {
        int result = Integer.parseInt(algorithm[algStep + 1]);
        ++algStep;
        return result;
      } catch (NumberFormatException e) { };
    return defaultValue;
  }

  private double birthday = System.currentTimeMillis() / 1000.0;

  public void run() {
    // Generate noise and apply filters
    for (algStep = algStart; algStep < algorithm.length; ++algStep) {
      String a = algorithm[algStep];
      if (a.equals("midpoint")) {
        terrain.generateMidpointDisplacementNoise(algParam(W/2), 
                                                  algParam(0.5));
        terrain.normalize();

      } else if (a.equals("perlin")) {
        double plane = algStep * 171.4934 + System.currentTimeMillis() / 1000.;
        terrain.generatePerlinNoise(plane, 
                                    algParam(64),
                                    algParam(1), 
                                    algParam(0.5));

      } else if (a.equals("gustav")) {
        final double plane = 
          ((algStep-1) * 1.4934 + 
           System.currentTimeMillis() / 1000. - birthday) % 2;
        terrain.filter(new Noise.Filter() { 
            public double filter(int x, int y, double v) {
              v = Gustavson.noise(x*1./W, y*1./H, plane); 
              return v;
            }});

      } else if (a.equals("spectral")) {
        terrain.generateSpectralNoise(algParam(50), 
                                      algParam(1.0), 
                                      algParam(32));
        terrain.normalize();

      } else if (a.equals("voronoi")) {
        terrain.generateVoronoi(algParam(10),
                                new double[]{algParam(-1.0), 
                                             algParam(1.0)});

      } else if (a.equals("white")) {
        terrain.generateWhiteNoise();
        terrain.normalize();

      } else if (a.equals("fault")) {
        terrain.generateFaultNoise(algParam(200));
        terrain.normalize();

      } else if (a.equals("grid")) {
        terrain.generateGrid();
        terrain.normalize();

      } else if (a.equals("smooth")) {
        terrain.smooth();

      } else if (a.equals("smoothmore")) {
        terrain.smoothMore();

      } else if (a.equals("normalize")) {
        terrain.normalize(algParam(-1.0), 
                          algParam(1.0));

      } else if (a.equals("binormalize")) {
        terrain.binormalize(algParam(-1.0), 
                            algParam(1.0));

      } else if (a.equals("fade")) {
        final double radius = algParam(10);
        terrain.filter(new Noise.Filter() { 
            public double filter(int x, int y, double v) {
              int d = Math.min(x, W-1-x);
              if (d < radius)
                v = (v + 1) * Math.sqrt(1 - Math.pow((d - radius)/radius, 2.0)) - 1;
              d = Math.min(y, H-1-y);
              if (d < radius)
                v = (v + 1) * Math.sqrt(1 - Math.pow((d - radius)/radius, 2.0)) - 1;
              return v;
            }});

      } else if (a.equals("thermal")) {
        for (int i = algParam(50); i > 0; --i)
          terrain.erode(algParam(6.0 / W), 1000);

      } else if (a.equals("olsen")) {
        for (int i = algParam(50); i > 0; --i)
          terrain.erode(0, algParam(8.0 / W));

      } else if (a.equals("distort")) {
        terrain.distortFractally(algParam(1/8.0), 
                                 algParam(64));
      } else if (a.equals("exp")) {
        terrain.exp(algParam(2.0));

      } else if (a.equals("abs")) {
        terrain.filter(new Noise.Filter() { 
            public double filter(int x, int y, double v){return Math.abs(v);}});

      } else if (a.equals("scale")) {
        final double factor = algParam(-1.0);
        terrain.filter(new Noise.Filter() { 
            public double filter(int x, int y, double v) { return factor*v; }});

      } else if (a.equals("bias")) {
        final double bias = algParam(1.0);
        terrain.filter(new Noise.Filter() { 
            public double filter(int x, int y, double v) { return bias + v; }});

      } else if (a.equals("step")) {
        final double cutoff = algParam(0.0);
        terrain.filter(new Noise.Filter() { 
            public double filter(int x, int y, double v) { 
              return v >= cutoff ? 1.0 : 0.0; }});

      } else if (a.equals("fill")) {
        terrain.fill();

      } else if (a.equals("flow")) {
        terrain.flow();

      } else if (a.equals("rain")) {
        terrain.hydraulicErosion(algParam(0.01), 
                                 algParam(0.01), 
                                 algParam(0.5), 
                                 algParam(0.01));

      } else if (a.equals("path")) {
        Random rand = new Random();
        Point start = new Point(rand.nextInt(terrain.W), 
                                rand.nextInt(terrain.H));
        Point goal = new Point(rand.nextInt(terrain.W), 
                               rand.nextInt(terrain.H));
        PathFinder pather = new PathFinder(terrain);
        List<Point> path = pather.findPath(start, goal);
        if (terrain.water == null) 
          terrain.water = new double[terrain.W * terrain.H];
        for (Point p : path)
          terrain.water[p.x + p.y * terrain.W] += 1;

      } else if (a.equals("histogram")) {
        final int[] bins = new int[terrain.W];
        for (int j = 0; j < terrain.H; ++j) {
          for (int i = 0; i < terrain.W; ++i) {
            int b = (int) ((terrain.noise(i,j) + 1) / 2.0 * terrain.W);
            if (b < 0) b = 0; else if (b >= terrain.W) b = terrain.W - 1;
            ++bins[b];
          }
        }
        int max = 0;
        for (int i = 0; i < terrain.W; ++i)
          if (max < bins[i])
            max = bins[i];
        final int fmax = max;
        terrain.filter(new Noise.Filter() { 
          public double filter(int x, int y, double v) { 
            if ((terrain.H-1-y) * fmax < bins[x] * terrain.H)
              return 2.0 * x / terrain.W - 1;
            else if (x < terrain.W/2)
              return 1;
            else 
              return -1;
          }});

      } else if (a.equals("push")) {
        if (stack == null) stack = new Stack<Terrain>();
        stack.push(terrain);
        terrain = new Terrain(W, H);

      } else if (a.equals("add")) {
        final Terrain popped = stack.pop();
        terrain.filter(new Noise.Filter() { 
          public double filter(int x, int y, double v) { 
            return v + popped.noise(x, y); }});

      } else if (a.equals("mul")) {
        final Terrain popped = stack.pop();
        terrain.filter(new Noise.Filter() { 
          public double filter(int x, int y, double v) { 
            return v * popped.noise(x, y); }});

        //} else if (a.equals("wait")) {
        //waitForKey();

      } else if (a.equals("--") || a.equals("loop")) {
        algStart = algStep + 1;

      } else {
        System.err.printf("ERROR: Bad parameter: %s\n", a);
        usage();
        System.exit(-1);
      }
    }
  }

  public void breakLoop() {
    for (algStart = Math.max(0, algStart-2); algStart > 0; --algStart)
      if (algorithm[algStart].equals("--") || algorithm[algStart].equals("loop"))
        break;
    for (; algStart < algorithm.length-1; ++algStart) try {
        Integer.parseInt(algorithm[algStart]);
      } catch (NumberFormatException e) { 
        break;
      };
  }

}
