import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.input.Keyboard;
 
public class Terrain3dTest {
 
  Algorithm algorithm;

  Boolean running = true;
  
  long now, before, start;

  float viewz = 8f;
  float yaw = 30f, pitch = 200f;
  float scale = 10f;
  float steepness = 1f;

  Font font = new Font();

  public Terrain3dTest(String[] args) {
    algorithm = new Algorithm(args);
  }


  private FloatBuffer buf(float f0, float f1, float f2, float f3) {
    return buf(new float[]{f0, f1, f2, f3});
  }

  private FloatBuffer buf(float[] fs) {
    FloatBuffer result = BufferUtils.createFloatBuffer(fs.length).put(fs);
    result.flip();
    return result;
  }


  public void start() {
    try {
      Display.setDisplayMode(new DisplayMode(800,600));
      Display.create();
      //Display.setVSyncEnabled(true);
    } catch (LWJGLException e) {
      e.printStackTrace();
      System.exit(0);
    }

    // Init OpenGL
    GL11.glMatrixMode(GL11.GL_PROJECTION);
    GL11.glLoadIdentity();
    GL11.glFrustum(-1f, 1f, -1f, 1f, 1f, 20f);
    GL11.glMatrixMode(GL11.GL_MODELVIEW);

    //GL11.glFrontFace(GL11.GL_CW);

    font.init();

    createLighting();

    int frames = 0;
    long lastreport = 0;
    before = start = System.currentTimeMillis();
    float fps = 0f;

    final float dx = 1f / algorithm.W;
    final float dz = 1f / algorithm.H;

    while (!Display.isCloseRequested() && running) {

      now = System.currentTimeMillis();

      algorithm.run();

      handleInput();

      // Start from scratch
      GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
      GL11.glLoadIdentity();

      // Show the sun for now...
      GL11.glColor3f(1,1,1);
      pyramid(-8,8,-10,1f);

      // Move the map back where we can see it
      GL11.glTranslatef(0f, 0f, -viewz);

      // Spin the map as user requests
      GL11.glRotatef(pitch, 1f, 0f, 0f);
      GL11.glRotatef(yaw, 0f, 1f, 0f);

      // Scale up the map
      GL11.glScalef(scale, scale * steepness, scale);

      // Center the map
      GL11.glTranslatef(-1/2f, 0, -1/2f);

      final float s = -0.25f;

      for (int i = 0; i < algorithm.W-1; ++i) {
        float x = i * dx;
        float oldheight = 0, oldnabe = 0;
        GL11.glBegin(GL11.GL_TRIANGLE_STRIP);
        for (int j = 0; j < algorithm.H; ++j) {
          float z = j * dz;

          float height = (float)algorithm.terrain().noise(i,j);     // h(x,   y)
          float neighbor = (float)algorithm.terrain().noise(i+1,j); // h(x+dx,y)

          colorize((oldnabe + oldheight + height) / 3);
          normal(s*oldnabe, s*oldheight, s*height);
          GL11.glVertex3f(x,    s * height, z);

          colorize((height + neighbor + oldnabe) / 3f);
          normal(s*height, s*neighbor, s*oldnabe);
          GL11.glVertex3f(x+dx, s * neighbor, z);

          oldheight = height;                                 // h(x,y-dy)
          oldnabe   = neighbor;                               // h(x+dx,y-dy)

          // oh on
          // nh nn
          //
          // (That stands for oldheight, newneighbor, etc. It's not
          // meant to be sexy sounds.)
        }
        GL11.glEnd();
      }

      GL11.glLoadIdentity();
      GL11.glScalef(1,-1,1);
      font.render(String.format("%3.0f fps", fps), -0.9f, 0.9f, -1.1f, 0.05f);

      Display.update();
      //Display.sync(60);

      // Report statistics
      ++frames;
      if (now - lastreport > 1000) {
        fps = frames * 1000f / (now-lastreport);
        System.out.printf("%3.0f fps %f %f° %f°\n", 
                          fps,
                          viewz,
                          yaw,
                          pitch);
        lastreport = now;
        frames = 0;
      }
      
      before = now;
    }
 
    Display.destroy();
    System.out.println("done");
  }


  private void colorize(float height) {
    // Convert height to ARGB 
    float R = 0, G = 0, B = 0;
    if (height < -0.5f) {
      B = (height + 1.0f) * 2.0f;
    } else if (height < 0) {
      G = (height + 0.5f) * 2.0f;
      B = 1.0f;
    } else  {
      R = height;
      G = 0.5f + height * 0.5f;
      // For white peaks:
      if (height > 0.75f)
        B = (height - 0.75f) * 4f;
    }
    GL11.glColor3f(R,G,B);
    //GL11.glMaterial(GL11.GL_FRONT, GL11.GL_AMBIENT_AND_DIFFUSE, buf(new float[]{R,G,B,1}));
  }


  private void normal(float c, float b, float a) {
    // Compute the normalized cross-product of vectors BA and BC where
    // A = <0, a,-1>
    // B = <0, b, 0>
    // C = <1, c, 0>
    // so
    // BA = <0, a-b, -1>
    // BC = <1, c-b,  0>
    // so
    // BA x BC = <(a-b) * 0 - (-1 * (c-b)),
    //            -1 * 1 - 0 * 0,
    //            0 * (c-b) - (a-b) * 1>
    // which equals
    // <c-b, -1, c-a>
    // Which should then be normalized if I turn off GL_NORMALIZE.
    // Magnitude = sqrt( cc -2bc +bb + 1 + cc - 2ac + aa )
    //           = sqrt( aa + bb + 2c(c-a-b) + 1 )
    float mag = (float) Math.sqrt(a*a + b*b + 2*c*(c-a-b) + 1);
    GL11.glNormal3f((c-b)/mag, -1/mag, (c-a)/mag);
  }


  private void pyramid(float x, float y, float z, float s) {
    GL11.glBegin(GL11.GL_TRIANGLES);

    GL11.glVertex3f(x,  y+s,z  );
    GL11.glVertex3f(x+s,y,  z  );
    GL11.glVertex3f(x,  y,  z+s);

    GL11.glVertex3f(x,  y,  z  );
    GL11.glVertex3f(x+s,y,  z  );
    GL11.glVertex3f(x,  y,  z+s);

    GL11.glVertex3f(x,  y+s,z  );
    GL11.glVertex3f(x  ,y,  z  );
    GL11.glVertex3f(x,  y,  z+s);

    GL11.glVertex3f(x,  y+s,z  );
    GL11.glVertex3f(x+s,y,  z  );
    GL11.glVertex3f(x,  y,  z  );

    GL11.glEnd();
  }


  private void createLighting() {
    GL11.glEnable(GL11.GL_LIGHTING);
    
    GL11.glEnable(GL11.GL_LIGHT0);

    GL11.glEnable(GL11.GL_COLOR_MATERIAL);
    GL11.glColorMaterial(GL11.GL_FRONT, GL11.GL_AMBIENT_AND_DIFFUSE);


    //GL11.glLightModel(GL11.GL_LIGHT_MODEL_AMBIENT, buf(0.1f,0.1f,0.5f,1f));


    float diffuse[] = {0.61424f, 0.04136f, 0.04136f, 1f};
    float specular[] = {0.727811f, 0.626959f, 0.626959f, 1f};

    GL11.glLightModeli(GL11.GL_LIGHT_MODEL_TWO_SIDE, GL11.GL_TRUE);

    GL11.glEnable(GL11.GL_DEPTH_TEST);
    GL11.glDepthFunc(GL11.GL_LESS);

    float position[] = {-8,8,-10,1f};
    GL11.glLight(GL11.GL_LIGHT0, GL11.GL_POSITION, buf(position));

    GL11.glLightModelf(GL11.GL_LIGHT_MODEL_LOCAL_VIEWER, 0f);



    //GL11.glEnable(GL11.GL_RESCALE_NORMAL); // wha? not defined?
    GL11.glEnable(GL11.GL_NORMALIZE); 
    //GL11.glShadeModel(GL11.GL_SMOOTH);

    GL11.glMaterial(GL11.GL_FRONT, GL11.GL_AMBIENT, buf(0, 0, 0.2f, 1));
    GL11.glMaterial(GL11.GL_FRONT, GL11.GL_DIFFUSE, buf(0.2f, 0, 0, 1));
    GL11.glMaterial(GL11.GL_FRONT, GL11.GL_SPECULAR, buf(0, 0.2f, 0, 1));
    GL11.glMateriali(GL11.GL_FRONT, GL11.GL_SHININESS, 90);

    //GL11.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    //GL11.glColor3f(1.0f, 1.0f, 1.0f);
  }


  private void handleInput() {
    while (Keyboard.next()) {
      if (Keyboard.getEventKeyState()) {
        // Key down

        // Check for exit
        if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE ||
            Keyboard.getEventKey() == Keyboard.KEY_C ||
            Keyboard.getEventKey() == Keyboard.KEY_Q)
          running = false;

        if (Keyboard.getEventKey() == Keyboard.KEY_R)
          algorithm.breakLoop();
      }
    }

    final float flyspeed = 2.5f * (now - before) / 1000f;
    final float spinspeed = 0.25f * (now - before) * 360f / 1000f;;
    final float scalespeed = (float) Math.pow(2, (now - before) / 1000.0);
    
    if (Keyboard.isKeyDown(Keyboard.KEY_W))      viewz -= flyspeed;
    if (Keyboard.isKeyDown(Keyboard.KEY_S))      viewz += flyspeed;

    if (Keyboard.isKeyDown(Keyboard.KEY_UP))     pitch += spinspeed;
    if (Keyboard.isKeyDown(Keyboard.KEY_DOWN))   pitch -= spinspeed;

    if (Keyboard.isKeyDown(Keyboard.KEY_LEFT))   yaw += spinspeed;
    if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT))  yaw -= spinspeed;

    if (Keyboard.isKeyDown(Keyboard.KEY_EQUALS)) steepness *= scalespeed;
    if (Keyboard.isKeyDown(Keyboard.KEY_MINUS))  steepness /= scalespeed;

    if (Keyboard.isKeyDown(Keyboard.KEY_T))
      algorithm.breakLoop();
  }


  public static void main(String[] argv) {
    Terrain3dTest app = new Terrain3dTest(argv);
    app.start();
  }
}
