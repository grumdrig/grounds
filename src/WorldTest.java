import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;


class WorldTest extends JPanel {
  
  public static void main(String[] args) {
    JFrame frame = new JFrame("World Map Test");
    JPanel panel = new WorldTest();
    frame.add(panel);
    frame.pack();
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private BufferedImage canvas;

  private int W = 256;
  private int H = 256;

  public WorldTest() {
    canvas = new BufferedImage(W, H, BufferedImage.TYPE_INT_ARGB);
    render();
  }

  int frames = 0;
  long lastreport = 0;

  private void render() {
    // Render noise
    double min = 0;
    double max = 0;
    long now = System.currentTimeMillis();
    double z = (now / 1000.0) / 1;
    for (int x = 0; x < W; x++) {
      double fadex = fadeEdge(x, W);
      for (int y = 0; y < H; y++) {
        double oheight = PerlinNoise.pinkNoise(x,y,z, 64,1, 0.5);
        double fadey = fadeEdge(y, H);
        double height = (oheight + 1) * fadey * fadex - 1;
        double foliage = PerlinNoise.noise(x / 16.0, y / 16.0, z + 1000);
        double latitude = 2 * ((double)y / H - 0.5);
        latitude *= latitude;
        double moisture = PerlinNoise.noise(x / 32.0, y / 32.0, z + 2000) + latitude;

        // Colorize the pixel
        // Convert height to ARGB as a simple black to white palette
        int scaled = heightToByte(height);//4 * height * height * height);
        int R=0, G=0, B=0;
        if (height < 0 && latitude - ((oheight + 1) * fadey - 1) > 1.4) {
          // ice 
          R = 64;
          G = B = scaled + 64;
        } else if (height < 0) {
          // sea
          B = scaled;
        } else if (height + latitude > 0.6) {
          // snow-capped mountains
          R = G = B = scaled;
        } else if (height < 0.02 && moisture < -0.3 ||
                   height < 0.4 && moisture < -0.2) {
          // sand
          R = G = Math.min(scaled+32, 255); B = 128;
        } else if (foliage < 0.15) {
          // grassy
          R = 3 * scaled/4;
          G = scaled;
        } else {
          // forest
          G = scaled - 64;
        }

        //R=G=B=heightToByte(latitude);
        canvas.setRGB(x, y, RGB(R,G,B));

        if (min > height) min = height;
        if (max < height) max = height;
      }
    }

    // Report statistics
    ++frames;
    if (now - lastreport > 1000) {
      System.out.printf("%.3f - %.3f %3.0f FPS\n", min, max, 
                        frames * 1000.0 / (now-lastreport));
      lastreport = now;
      frames = 0;
    }
    repaint();
  }


  int heightToByte(double height) {
    // Convert from [-1.0, 1.0) to [0,256)
    // (Should properly be scaled by 128, I think, but noise function doesn't
    // generally reach the extreme ends, so using 160 to make it more dynamic.)
    int scaled = (int) (height * 160 + 128);
    if (scaled < 0) scaled = 0;
    else if (scaled > 255) scaled = 255;
    return scaled;
  }
      
  double fadeEdge(int x, int max) {
    double fade = ((x < max/2) ? x : max - x) / (max/2.0);
    fade = 1 - fade;
    fade *= fade * fade * fade;
    fade = 1 - fade;
    return fade;
  }

  private int RGB(int r, int g, int b) {
    return 0xFF000000 | r << 16 | g << 8 | b;
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    g2.drawImage(canvas, 0, 0, getWidth(), getHeight(), 0, 0, W, H, null);
    render();
  }

  public Dimension getPreferredSize() {
    return new Dimension(W, H);
  }
}
