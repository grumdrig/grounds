//
// Description : Array and textureless GLSL 2D/3D/4D simplex 
//               noise functions.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : ijm
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
// 

class Gustavson {
  private static class vec3 {
    public double x, y, z;
    public vec3(double x, double y, double z) {
      this.x = x;
      this.y = y;
      this.z = z;
    }
    public vec3 yzx() { return new vec3(y,z,x); }
    public vec3 zxy() { return new vec3(z,x,y); }
    public vec3 plus(double v) { return new vec3(x + v, y + v, z + v);}
    public vec3 times(double m) { return new vec3(m * x, m * y, m * z); }
    public vec3 minus(vec3 v) { return new vec3(x - v.x, y - v.y, z - v.z); }
    public vec3 plus(vec3 v) { return new vec3(x + v.x, y + v.y, z + v.z); }
  }
  public static vec3 floor(vec3 v) { 
    return new vec3(Math.floor(v.x), Math.floor(v.y), Math.floor(v.z)); }
  public static double dot(vec3 v, vec3 w) { 
    return v.x * w.x + v.y * w.y + v.z * w.z; }
  public static vec3 step(vec3 v, vec3 w) {
    return new vec3(v.x < w.x ? 0. : 1.,
                    v.y < w.y ? 0. : 1.,
                    v.z < w.z ? 0. : 1.); }
  public static vec3 min(vec3 v, vec3 w) {
    return new vec3(v.x < w.x ? v.x : w.x,
                    v.y < w.y ? v.y : w.y,
                    v.z < w.z ? v.z : w.z); }
  public static vec3 max(vec3 v, vec3 w) {
    return new vec3(v.x > w.x ? v.x : w.x,
                    v.y > w.y ? v.y : w.y,
                    v.z > w.z ? v.z : w.z); }

  private static class vec4 {
    public double x, y, z, w;
    public vec4(double x, double y, double z, double w) {
      this.x = x;
      this.y = y;
      this.z = z;
      this.w = w;
    }
    public vec4 xzyw() { return new vec4(x,z,y,w); }
    public vec4 xxyy() { return new vec4(x,x,y,y); }
    public vec4 zzww() { return new vec4(z,z,w,w); }
    public vec4 plus(double v) { return new vec4(x + v, y + v, z + v, w + v);}
    public vec4 times(double m) { return new vec4(m * x, m * y, m * z, m * w);}
    public vec4 minus(vec4 v) { return new vec4(x-v.x, y-v.y, z-v.z, w-v.w); }
    public vec4 times(vec4 v) { return new vec4(x*v.x, y*v.y, z*v.z, w*v.w); }
    public vec4 plus(vec4 v) { return new vec4(x+v.x, y+v.y, z+v.z, w+v.w); }
  }
  public static vec4 floor(vec4 v) { 
    return new vec4(Math.floor(v.x), Math.floor(v.y), 
                    Math.floor(v.z), Math.floor(v.w)); }
  public static vec4 abs(vec4 v) { 
    return new vec4(Math.abs(v.x), Math.abs(v.y), 
                    Math.abs(v.z), Math.abs(v.w)); }
  public static vec4 step(vec4 v, vec4 w) {
    return new vec4(v.x < w.x ? 0. : 1.,
                    v.y < w.y ? 0. : 1.,
                    v.z < w.z ? 0. : 1.,
                    v.w < w.w ? 0. : 1.); }
  public static vec4 max(double w, vec4 v) {
    return new vec4(v.x > w ? v.x : w,
                    v.y > w ? v.y : w,
                    v.z > w ? v.z : w,
                    v.w > w ? v.w : w); }
  public static double dot(vec4 v, vec4 w) { 
    return v.x * w.x + v.y * w.y + v.z * w.z + v.w * w.w; }


  private static vec3 mod289(vec3 x) {
    return x.minus(floor(x.times(1/289.)).times(289.));
  }

  private static vec4 mod289(vec4 x) {
    return x.minus(floor(x.times(1/289.)).times(289.));
  }

  private static vec4 permute(vec4 x) {
    return mod289(x.times(34.).plus(1.).times(x));
  }

  private static vec4 taylorInvSqrt(vec4 r) {
    return r.times(-0.85373472095314).plus(1.79284291400159);
  }

  public static double noise(double vx, double vy, double vz) { 
    final vec3 v =  new vec3(vx,vy,vz);

    final double Cx = 1/6., Cy = 1/3.;
    final vec3 Cxxx = new vec3(Cx, Cx, Cx), Cyyy = new vec3(Cy, Cy, Cy);
    final double Dx = 0.0, Dy = 0.5, Dz = 1.0, Dw = 2.0;
    final vec3 Dyyy = new vec3(Dy,Dy,Dy);

    // First corner
    vec3 i  = floor(v.plus(dot(v, Cyyy)));
    vec3 x0 = v.minus(i.plus(dot(i, Cxxx)));
    
    // Other corners
    vec3 g = step(x0.yzx(), x0);
    vec3 l = g.times(-1.).plus(1.);
    vec3 i1 = min(g, l.zxy());
    vec3 i2 = max(g, l.zxy());
    
    vec3 x1 = x0.minus(i1).plus(Cx);
    vec3 x2 = x0.minus(i2).plus(Cy);
    vec3 x3 = x0.plus(-Dy);
    
    // Permutations
    i = mod289(i); 
    vec4 p = permute( permute( permute(
                                 new vec4(0, i1.z, i2.z, 1).plus(i.z))
                           .plus(new vec4(0, i1.y, i2.y, 1).plus(i.y)))
                           .plus(new vec4(0, i1.x, i2.x, 1).plus(i.x)));
    
    // Gradients: 7x7 points over a square, mapped onto an octahedron.
    // The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
    final double n_ = 1/7.;
    vec3  ns = new vec3(Dw,Dy,Dz).times(n_).minus(new vec3(Dx,Dz,Dx));
    
    vec4 j = p.plus(floor(p.times(ns.z * ns.z)).times(-49.));
    
    vec4 x_ = floor(j.times(ns.z));
    vec4 y_ = floor(j.plus(x_.times(-7.)));
    
    vec4 x = x_.times(ns.x).plus(ns.y);
    vec4 y = y_.times(ns.x).plus(ns.y);
    vec4 h = abs(x).plus(abs(y)).times(-1.).plus(1.);
    
    vec4 b0 = new vec4(x.x, x.y, y.x, y.y);
    vec4 b1 = new vec4(x.z, x.w, y.z, y.w);
    
    vec4 s0 = floor(b0).times(2.).plus(1.);
    vec4 s1 = floor(b1).times(2.).plus(1.);
    vec4 sh = step(h, new vec4(0,0,0,0)).times(-1);
    
    vec4 a0 = b0.xzyw().plus(s0.xzyw().times(sh.xxyy()));
    vec4 a1 = b1.xzyw().plus(s1.xzyw().times(sh.zzww()));
    
    vec3 p0 = new vec3(a0.x, a0.y, h.x);
    vec3 p1 = new vec3(a0.z, a0.w, h.y);
    vec3 p2 = new vec3(a1.x, a1.y, h.z);
    vec3 p3 = new vec3(a1.z, a1.w, h.w);
    
    //Normalise gradients
    vec4 norm = taylorInvSqrt(new vec4(dot(p0,p0), dot(p1,p1), 
                                       dot(p2, p2), dot(p3,p3)));
    p0 = p0.times(norm.x);
    p1 = p1.times(norm.y);
    p2 = p2.times(norm.z);
    p3 = p3.times(norm.w);
    
    // Mix final noise value
    vec4 m = max(0,
                 new vec4(dot(x0,x0), dot(x1,x1), 
                          dot(x2,x2), dot(x3,x3)).times(-1).plus(0.6));
    m = m.times(m);
    return 42.0 * dot(m.times(m), 
                      new vec4(dot(p0,x0), dot(p1,x1), dot(p2,x2),dot(p3,x3)));
  }

  public static void main(String args[]) {
    double plane = 0.;
    System.out.printf("%f\n", noise(0,0,plane));
    System.out.printf("%f\n", noise(1,0,plane));
    System.out.printf("%f\n", noise(0,1,plane));
  }
}
