import java.io.IOException;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class FontTest {
  
  /** The texture that will hold the image details */
  private Texture texture;
  
  
  /**
   * Start the example
   */
  public void start() {
    initGL(800,600);
    init();
    
    while (true) {
      GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

      render("Good Evening, Prof. Falken!", 100, 100, 10);
      render("Would you like to play a game?", 100, 140, 50);
      render("How 'bout Global Thermonuclear War?", 100, 220, 100);
      
      Display.update();
      Display.sync(100);

      if (Display.isCloseRequested()) {
        Display.destroy();
        System.exit(0);
      }
    }
  }
  
  /**
   * Initialise the GL display
   * 
   * @param width The width of the display
   * @param height The height of the display
   */
  private void initGL(int width, int height) {
    try {
      Display.setDisplayMode(new DisplayMode(width,height));
      Display.create();
      Display.setVSyncEnabled(true);
    } catch (LWJGLException e) {
      e.printStackTrace();
      System.exit(0);
    }

    GL11.glEnable(GL11.GL_TEXTURE_2D);               
        
    GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);          
        
    // enable alpha blending
    GL11.glEnable(GL11.GL_BLEND);
    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        
    GL11.glViewport(0,0,width,height);
    GL11.glMatrixMode(GL11.GL_MODELVIEW);

    GL11.glMatrixMode(GL11.GL_PROJECTION);
    GL11.glLoadIdentity();
    GL11.glOrtho(0, width, height, 0, 1, -1);
    GL11.glMatrixMode(GL11.GL_MODELVIEW);
  }
  
  /**
   * Initialise resources
   */
  public void init() {
    
    try {
      // load texture from PNG file
      texture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("font.png"));
      
      System.out.println("Texture loaded: "+texture);
      System.out.println(">> Image width: "+texture.getImageWidth());
      System.out.println(">> Image height: "+texture.getImageHeight());
      System.out.println(">> Texture width: "+texture.getTextureWidth());
      System.out.println(">> Texture height: "+texture.getTextureHeight());
      System.out.println(">> Texture ID: "+texture.getTextureID());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void render(String text, float x, float y, float size) {
    Color.white.bind();
    texture.bind(); // or GL11.glBind(texture.getTextureID());

    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, 
                         GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, 
                         GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
    //GL11.glTexParameteri(GL11.GL_TEXTURE_2D, 
    //                     GL11.GL_TEXTURE_WRAP_S, GL11.GL_CLAMP);
    //GL11.glTexParameteri(GL11.GL_TEXTURE_2D, 
    //                     GL11.GL_TEXTURE_WRAP_T, GL11.GL_CLAMP);

    GL11.glPushMatrix();

    GL11.glTranslatef(x,y,0);
    GL11.glScalef(size,size,0);

    for (int i = 0; i < text.length(); ++i)
      renderChar(text.charAt(i));

    GL11.glPopMatrix();
  }

  private void renderChar(char ch) {
    final String chars = 
      "ABCDEFGHIJ" + 
      "KLMNOPQRST" + 
      "UVWXYZ.,:;" +
      "1234567890" +
      "?!-+*#$%/\\" +
      "abcdefghij" +
      "klmnopqrst" +
      "uvwxyz~'`\"" +
      "          " +
      "X         ";
    
    int i,j,c = chars.indexOf(ch);
    if (c == -1) {
      i = 0;
      j = 9; 
    } else {
      i = c % 10;
      j = c / 10;
    }
    float d = 6f/64f;
    float x = i * d;
    float y = j * d;
    
    GL11.glBegin(GL11.GL_QUADS);

    GL11.glTexCoord2f(x, y);
    GL11.glVertex2f(0, 0);

    GL11.glTexCoord2f(x+d, y);
    GL11.glVertex2f(1, 0);

    GL11.glTexCoord2f(x+d, y+d);
    GL11.glVertex2f(1, 1);

    GL11.glTexCoord2f(x, y+d);
    GL11.glVertex2f(0, 1);
    
    GL11.glEnd();

    GL11.glTranslatef(1,0,0);
  }
  
  /**
   * Main Class
   */
  public static void main(String[] argv) {
    FontTest textureExample = new FontTest();
    textureExample.start();
  }
}