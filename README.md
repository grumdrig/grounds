Routines for generating terrain maps
====================================


Go!
---

To build & get usage info:
$ ant
$ java -cp build TerrainTest



Pink Noise
----------

Perlin Noise:

Perlin noise is a relatively smooth 3D mesh with details at a
particular level of scale. By combining Perlin noise at a range of
octaves 1/f noise can be generated.

+ Point-independent (Can be computed independently at any x,y)
+ Can be continuously morphed
+ Detail can be controlled
+ Dynamic LOD
- Somewhat slower than midpoint displacement

Midpoint displacement:

+ Faster than Perlin Noise
- Requires precomputation of surrounding terrain

Fault line:

+ Point-independent
+ Kind of interesting
- Obvious artifacts



Filters
-------

Normalize

Smooth


Testing
-------

Development testing for 3d view:

./test.sh perlin


TODO
----

- Convert to vertex buffers
- Fix the damned lighting between strips (probably miscalced normals)
- Move the light somewhere better
